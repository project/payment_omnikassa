<?php

namespace Drupal\Tests\payment_omnikassa\Functional;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Tests\BrowserTestBase;

/**
 * Basic testing of the Payment Rabo Omnikassa module.
 *
 * @group payment_omnikassa
 */
class PaymentOmnikassaTest extends BrowserTestBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Admin user.
   *
   * @var \Drupal\user\Entity\User|bool
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'payment',
    'payment_omnikassa',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() :void {
    parent::setUp();

    // Setup and login admin user.
    $permissions = [
      'access administration pages',
      'administer modules',
      'administer site configuration',
      'payment.payment.view.any',
      'payment.payment_method_configuration.update.any',
      'payment.payment_method_configuration.delete.any',
      'payment.payment_method_configuration.view.any',
      'payment.payment_type.administer',
      'payment.payment_method_configuration.create.payment_omnikassa',
      'payment_omnikassa.payment.administer',
      'currency.currency.create',
    ];
    $this->adminUser = $this->drupalCreateUser($permissions);
    $this->drupalLogin($this->adminUser);

    // Add a currency.
    $this->drupalGet('/admin/config/regional/currency/import');
    $edit = [
      'edit-currency-code' => 'EUR'
    ];
    $this->submitForm($edit, $this->t('Import'));
  }

  /**
   * Test to add a Rabo Omnikassa profile.
   */
  public function testCRUDProfile() {
    // Add a profile.
    $this->drupalGet('/admin/config/services/payment/payment_omnikassa/profiles/add');
    $this->assertSession()->statusCodeEquals(200);
    $edit = [
      'edit-label' => 'Test Rabo Omnikassa',
      'id' => 'test_rabo_omnikassa',
      'edit-signingtoken' => 'Test Signing Token',
      'edit-signingkey' => 'Test Signing Key',
      'edit-paymenttypes-ideal' => 'IDEAL',
      'edit-servertypes' => 'https://betalen.rabobank.nl/omnikassa-api-sandbox',
    ];
    $this->submitForm($edit, $this->t('Save'));
    $this->assertSession()->responseContains($this->t('Test Rabo Omnikassa'));
    $this->assertSession()->responseContains($this->t('Edit'));

    // Create a Payment config with this profile.
    $this->drupalGet('/admin/config/services/payment/method/configuration-add/payment_omnikassa');
    $this->assertSession()->statusCodeEquals(200);
    $edit = [
      'edit-label' => 'Test Rabo Omnikassa method',
      'edit-id' => 'test_rabo_omnikassa_method',
      'edit-plugin-form-message-value' => 'Payment form message',
      'edit-plugin-form-plugin-form-brand-label' => 'Label Rabo Omnikassa',
      'edit-plugin-form-plugin-form-profile' => 'test_rabo_omnikassa'
    ];
    $this->submitForm($edit, $this->t('Save'));
    $this->assertSession()->responseContains($this->t('Test Rabo Omnikassa method'));

    // Edit this profile.
    $this->drupalGet('/admin/config/services/payment/payment_omnikassa/profiles/edit/test_rabo_omnikassa');
    $this->assertSession()->statusCodeEquals(200);
    $edit = [
      'edit-label' => 'Test Rabo Omnikassa 2',
    ];
    $this->submitForm($edit, $this->t('Save'));
    $this->assertSession()->responseContains($this->t('Test Rabo Omnikassa 2'));
    
    // Delete this profile.
    $this->drupalGet('/admin/config/services/payment/payment_omnikassa/profiles/edit/test_rabo_omnikassa');
    $this->assertSession()->statusCodeEquals(200);
    $this->clickLink($this->t('Delete'));
    $this->assertSession()->statusCodeEquals(200);
    $this->submitForm([], $this->t('Delete'));
    $this->assertSession()->responseContains($this->t('<em class="placeholder">Test Rabo Omnikassa 2</em> has been deleted.'));
  }

}
