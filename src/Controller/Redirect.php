<?php

namespace Drupal\payment_omnikassa\Controller;

/**
 * @file
 * Handles the return and IPN-updates for Rabo Omnikassa.
 */

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Url;
use Drupal\payment\Entity\Payment;
use Drupal\payment\Entity\PaymentInterface;
use Drupal\payment_omnikassa\Plugin\Payment\Method\InMemoryTokenProvider;
use Drupal\payment_omnikassa\Plugin\Payment\Method\PaymentOmnikassaPayment;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use nl\rabobank\gict\payments_savings\omnikassa_sdk\endpoint\Endpoint;
use nl\rabobank\gict\payments_savings\omnikassa_sdk\model\response\AnnouncementResponse;
use nl\rabobank\gict\payments_savings\omnikassa_sdk\model\signing\InvalidSignatureException;
use nl\rabobank\gict\payments_savings\omnikassa_sdk\model\signing\SigningKey;
use nl\rabobank\gict\payments_savings\omnikassa_sdk\model\response\PaymentCompletedResponse;

/**
 * Handles the "redirect" route.
 */
class Redirect extends ControllerBase {

  use MessengerTrait;

  /**
   * Rabo Omnikassa is redirecting the visitor here after the payment.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response.
   * @throws \Exception
   */
  public static function execute(Request $request) {
    // Check if this is a normal return after payment.
    $order_id = $request->query->get('order_id');
    if (isset($order_id)) {
      \Drupal::logger('payment_omnikassa')->notice('Return from payment provider for order: @order_id.',
        [
          '@order_id' => $order_id,
        ]
      );

      /** @var \Drupal\payment\Entity\Payment $payment */
      $payment = Payment::load($order_id);

      if ($payment === NULL) {
        \Drupal::logger('payment_omnikassa')->error('No payment found for order: @order_id.',
          [
            '@order_id' => $order_id,
          ]
        );
        \Drupal::messenger()->addStatus(\Drupal::translation()->translate('No payment found'));
        $options['absolute'] = TRUE;
        return new RedirectResponse(Url::fromRoute('<front>', [], $options)->toString(), 302);
      }
      Redirect::proccessPayment($payment, $request);
      $payment_type = $payment->getPaymentType();
      return $payment_type->getResumeContextResponse()->getResponse();
    }

    // Check if this is an IPN-call with updates.
    $json = $request->getContent();

    if ($json != '' && strpos($json, 'authentication') !== FALSE) {
      \Drupal::logger('payment_omnikassa')->notice('IPN-update received from Rabo Omnikassa.');
      $profiles = Redirect::getAllPaymentOmnikassaProfiles();

      /** @var \Drupal\payment_omnikassa\Entity\PaymentOmnikassaProfileInterface $profile */
      foreach ($profiles as $profile) {
        $signingKey = new SigningKey(base64_decode($profile->getSigningKey()));
        $inMemoryTokenProvider = new InMemoryTokenProvider($profile->getSigningToken());
        $endpoint = Endpoint::createInstance($profile->getServerType(), $signingKey, $inMemoryTokenProvider);

        try {
          $announcementResponse = new AnnouncementResponse($json, $signingKey);
          do {
            $response = $endpoint->retrieveAnnouncement($announcementResponse);
            $results = $response->getOrderResults();

            foreach ($results as $result) {
              /** @var \Drupal\payment\Entity\Payment $payment */
              $payment = Payment::load($result->getMerchantOrderId());

              if ($payment === NULL) {
                \Drupal::logger('payment_omnikassa')
                  ->error('IPN-update: no payment found for order: @order_id.',
                    [
                      '@order_id' => $order_id,
                    ]
                  );
                $options['absolute'] = TRUE;
                return new RedirectResponse(Url::fromRoute('<front>', [], $options)->toString(), 302);
              }
              $new_status = $result->getOrderStatus();
              $payment_method = $payment->getPaymentMethod();

              // Check if order was completed. Other options are CANCELLED or EXPIRED.
              if ($new_status != 'COMPLETED') {
                $payment_method->updatePaymentStatus($payment, 'payment_cancelled');
              }
              else {
                $payment_method->updatePaymentStatus($payment, 'payment_success');
              }
              \Drupal::logger('payment_omnikassa')
                ->notice('IPN-update processed for order: @order_id. New status: @status.',
                  [
                    '@order_id' => $order_id,
                    '@status' => $new_status,
                  ]
                );
            }
          }
          while ($response->isMoreOrderResultsAvailable());

          $options['absolute'] = TRUE;
          return new RedirectResponse(Url::fromRoute('<front>', [], $options)->toString(), 200);
        }
        catch (InvalidSignatureException $e) {
          // Signature was invalid: the message was for another profile.
        }
      }
    }
    $options['absolute'] = TRUE;
    return new RedirectResponse(Url::fromRoute('<front>', [], $options)->toString(), 302);
  }

  /**
   * Process Payment on return from Rabo Omnikassa.
   *
   * @param \Drupal\payment\Entity\PaymentInterface $payment
   *   The payment.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @throws \Exception
   */
  public static function proccessPayment(PaymentInterface $payment, Request $request): void {
    $payment_method = $payment->getPaymentMethod();
    if (!$payment_method instanceof PaymentOmnikassaPayment) {
      throw new \Exception('$payment->getPaymentMethod() is not an instance of PaymentOmnikassaPayment.');
    }
    $payment_omnikassa_profile = $payment_method->getProfile();
    $order_id = $payment->id();
    $status = $request->query->get('status');
    $signature = $request->query->get('signature');
    $signing_key = new SigningKey(base64_decode($payment_omnikassa_profile->getSigningKey()));
    // Validate and sanitize response with PaymentCompletedResponse.
    $paymentCompletedResponse = PaymentCompletedResponse::createInstance($order_id, $status, $signature, $signing_key);
    if (!$paymentCompletedResponse) {
      $payment_method->updatePaymentStatus($payment, 'payment_pending');
      \Drupal::logger('payment_omnikassa')->error('Payment response validation failed.
      Orderid: @order_id.
      Status: @status.
      Signature: @signature',
        [
          '@order_id' => $order_id,
          '@status' => $status,
          '@signature' => $signature,
        ]
      );
    }
    else {
      // Check if order was completed. Other options are CANCELLED or EXPIRED.
      if ($paymentCompletedResponse->getStatus() != 'COMPLETED') {
        $payment_method->updatePaymentStatus($payment, 'payment_cancelled');
        \Drupal::logger('payment_omnikassa')->notice('Payment is cancelled by user.
        Orderid: @order_id.
        Status: @status.',
          [
            '@order_id' => $order_id,
            '@status' => $status,
          ]
        );
      }
      else {
        $payment_method->updatePaymentStatus($payment, 'payment_success');
      }
    }
  }

  /**
   * Find all available Rabo Omnikassa profiles.
   */
  public static function getAllPaymentOmnikassaProfiles() {
    $storage_manager = \Drupal::entityTypeManager()->getStorage('payment_omnikassa_profile');
    $profile_ids = \Drupal::entityQuery('payment_omnikassa_profile')->execute();
    return $storage_manager->loadMultiple($profile_ids);
  }

}
