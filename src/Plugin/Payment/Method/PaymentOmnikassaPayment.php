<?php

namespace Drupal\payment_omnikassa\Plugin\Payment\Method;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Url;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Utility\Token;
use Drupal\payment\Entity\Payment;
use Drupal\payment\Entity\PaymentInterface;
use Drupal\payment\EventDispatcherInterface;
use Drupal\payment\Plugin\Payment\Status\PaymentStatusManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\payment\Plugin\Payment\Method\PaymentMethodBase;
use Drupal\payment\Plugin\Payment\Method\PaymentMethodUpdatePaymentStatusInterface;
use Drupal\payment_omnikassa\Entity\PaymentOmnikassaProfile;
use nl\rabobank\gict\payments_savings\omnikassa_sdk\model\Money;
use nl\rabobank\gict\payments_savings\omnikassa_sdk\model\OrderItem;
use nl\rabobank\gict\payments_savings\omnikassa_sdk\model\ProductType;
use nl\rabobank\gict\payments_savings\omnikassa_sdk\model\request\MerchantOrder;
use nl\rabobank\gict\payments_savings\omnikassa_sdk\model\signing\SigningKey;
use nl\rabobank\gict\payments_savings\omnikassa_sdk\endpoint\Endpoint;

/**
 * A payment method using Rabo Omnikassa.
 *
 * Plugins that extend this class must have the following keys in their plugin
 * definitions:
 * - entity_id: (string) The ID of the payment method entity the plugin is for.
 * - execute_status_id: (string) The ID of the payment status plugin to set at
 *   payment execution.
 * - capture: (boolean) Whether or not payment capture is supported.
 * - capture_status_id: (string) The ID of the payment status plugin to set at
 *   payment capture.
 * - refund: (boolean) Whether or not payment refunds are supported.
 * - refund_status_id: (string) The ID of the payment status plugin to set at
 *   payment refund.
 *
 * @PaymentMethod(
 *   deriver = "Drupal\payment_omnikassa\Plugin\Payment\Method\PaymentOmnikassaPaymentDeriver",
 *   id = "payment_omnikassa",
 *   operations_provider = "\Drupal\payment_omnikassa\Plugin\Payment\Method\PaymentOmnikassaPaymentOperationsProvider",
 * )
 */
class PaymentOmnikassaPayment extends PaymentMethodBase implements PaymentMethodUpdatePaymentStatusInterface {

  use MessengerTrait;

  /**
   * The payment status manager.
   *
   * @var \Drupal\payment\Plugin\Payment\Status\PaymentStatusManagerInterface
   */
  protected $paymentStatusManager;

  /**
   * Constructs a new instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\payment\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   * @param \Drupal\Core\Utility\Token $token
   *   The token API.
   * @param \Drupal\payment\Plugin\Payment\Status\PaymentStatusManagerInterface $payment_status_manager
   *   The payment status manager.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, ModuleHandlerInterface $module_handler, EventDispatcherInterface $event_dispatcher, Token $token, PaymentStatusManagerInterface $payment_status_manager) {
    $configuration += $this->defaultConfiguration();
    parent::__construct($configuration, $plugin_id, $plugin_definition, $module_handler, $event_dispatcher, $token, $payment_status_manager);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition, $container->get('module_handler'), $container->get('payment.event_dispatcher'), $container->get('token'), $container->get('plugin.manager.payment.status'));
  }

  /**
   * {@inheritdoc}
   */
  public function getSupportedCurrencies() {
    return TRUE;
  }

  /**
   * Gets the ID of the payment method this plugin is for.
   *
   * @return string
   *   The entity id.
   */
  public function getEntityId() {
    return $this->pluginDefinition['entity_id'];
  }

  /**
   * Gets the status to set on payment execution.
   *
   * @return string
   *   The plugin ID of the payment status to set.
   */
  public function getExecuteStatusId() {
    return $this->pluginDefinition['execute_status_id'];
  }

  /**
   * Gets the status to set on payment capture.
   *
   * @return string
   *   The plugin ID of the payment status to set.
   */
  public function getCaptureStatusId() {
    return $this->pluginDefinition['capture_status_id'];
  }

  /**
   * Gets whether or not capture is supported.
   *
   * @return bool
   *   Whether or not to support capture.
   */
  public function getCapture() {
    return $this->pluginDefinition['capture'];
  }

  /**
   * Gets the status to set on payment refund.
   *
   * @return string
   *   The plugin ID of the payment status to set.
   */
  public function getRefundStatusId() {
    return $this->pluginDefinition['refund_status_id'];
  }

  /**
   * Gets whether or not capture is supported.
   *
   * @return bool
   *   Whether or not to support capture.
   */
  public function getRefund() {
    return $this->pluginDefinition['refund'];
  }

  /**
   * Get the Rabo Omnikassa profile to use.
   *
   * @return \Drupal\payment_omnikassa\Entity\PaymentOmnikassaProfile
   *   The Rabo Omnikassa Profile.
   */
  public function getProfile() {
    $profile_id = $this->pluginDefinition['profile'];
    return PaymentOmnikassaProfile::load($profile_id);
  }

  /**
   * {@inheritdoc}
   */
  protected function doExecutePayment() {
    // Get the Rabo Omnikassa profile.
    $payment_omnikassa_profile = $this->getProfile();

    // Create a Rabo Omnikassa payment.
    $order = $this->buildOmnikassaOrder($this->getPayment());

    $signingKey = new SigningKey(base64_decode($payment_omnikassa_profile->getSigningKey()));
    $inMemoryTokenProvider = new InMemoryTokenProvider($payment_omnikassa_profile->getSigningToken());
    $endpoint = Endpoint::createInstance($payment_omnikassa_profile->getServerType(), $signingKey, $inMemoryTokenProvider);
    $redirectUrl = $endpoint->announceMerchantOrder($order);

    try {
      $response = new TrustedRedirectResponse($redirectUrl);
      $response->send();
    }
    catch (\Exception $e) {
      $this->messenger()->addError($e->getMessage());
      return;
    }

  }

  /**
   * {@inheritdoc}
   */
  public function doCapturePayment() {
    $this->getPayment()->setPaymentStatus($this->paymentStatusManager->createInstance($this->getCaptureStatusId()));
    $this->getPayment()->save();
  }

  /**
   * {@inheritdoc}
   */
  public function doCapturePaymentAccess(AccountInterface $account) {
    return $this->getCapture() && $this->getPayment()->getPaymentStatus()->getPluginId() != $this->getCaptureStatusId();
  }

  /**
   * {@inheritdoc}
   */
  public function doRefundPayment() {
    $this->getPayment()->setPaymentStatus($this->paymentStatusManager->createInstance($this->getRefundStatusId()));
    $this->getPayment()->save();
  }

  /**
   * {@inheritdoc}
   */
  public function doRefundPaymentAccess(AccountInterface $account) {
    return $this->getRefund() && $this->getPayment()->getPaymentStatus()->getPluginId() != $this->getRefundStatusId();
  }

  /**
   * {@inheritdoc}
   */
  public function updatePaymentStatusAccess(AccountInterface $account) {
    return AccessResult::forbidden();
  }

  /**
   * {@inheritdoc}
   */
  public function getSettablePaymentStatuses(AccountInterface $account, PaymentInterface $payment) {
    return [];
  }

  /**
   * Update payment status,.
   */
  public function updatePaymentStatus(Payment $payment, $payment_status) {
    $payment->setPaymentStatus($payment->getPaymentMethod()->paymentStatusManager->createInstance($payment_status));
    $payment->save();
  }

  /**
   * Function creates a valid Rabo Omnikassa order.
   *
   * @param \Drupal\payment\Entity\Payment $payment
   *   Payment
   *
   * @return \nl\rabobank\gict\payments_savings\omnikassa_sdk\model\request\MerchantOrder
   *   Valid order.
   */
  private function buildOmnikassaOrder(Payment $payment) {

    $total = $payment->getAmount();

    foreach ($payment->getLineItems() as $line_item) {
      $order_items[] = OrderItem::createFrom([
        'name' => $line_item->getName(),
        'description' => $line_item->getDescription(),
        'quantity' => $line_item->getQuantity(),
        'amount' => Money::fromDecimal($line_item->getCurrencyCode(), $line_item->getTotalAmount()),
        'category' => ProductType::DIGITAL,
      ]);
    }

    $return_url = Url::fromRoute('payment_omnikassa.redirect',
      ['orderID' => $payment->id()], ['absolute' => TRUE])->toString();

    $site_config = \Drupal::config('system.site');
    $data = [
      'merchantOrderId' => $payment->id(),
      'description' => $site_config->get('name') . ' order ' . $payment->id(),
      'amount' => Money::fromDecimal($payment->getCurrencyCode(), $total),
      'merchantReturnURL' => $return_url,
    ];
    if (isset($order_items)) {
      $data['orderItems'] = $order_items;
    }

    return MerchantOrder::createFrom($data);
  }

}
