<?php

namespace Drupal\payment_omnikassa\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Defines Rabo Omnikassa profiles.
 */
interface PaymentOmnikassaProfileInterface extends ConfigEntityInterface {

}
