<?php

namespace Drupal\payment_omnikassa\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Defines a Rabo Omnikassa profile entity.
 *
 * @ConfigEntityType(
 *   admin_permission = "payment_omnikassa.payment.administer",
 *   handlers = {
 *     "access" = "\Drupal\Core\Entity\EntityAccessControlHandler",
 *     "form" = {
 *       "default" = "Drupal\payment_omnikassa\Entity\PaymentOmnikassaProfile\PaymentOmnikassaProfileForm",
 *       "delete" = "Drupal\payment_omnikassa\Entity\PaymentOmnikassaProfile\PaymentOmnikassaProfileDeleteForm"
 *     },
 *     "list_builder" = "Drupal\payment_omnikassa\Entity\PaymentOmnikassaProfile\PaymentOmnikassaProfileListBuilder",
 *     "storage" = "\Drupal\Core\Config\Entity\ConfigEntityStorage"
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "paymentTypes",
 *     "signingToken",
 *     "signingKey",
 *     "serverType",
 *     "uuid"
 *   },
 *   id = "payment_omnikassa_profile",
 *   label = @Translation("Rabo Omnikassa Profile"),
 *   links = {
 *     "canonical" = "/admin/config/services/payment/payment_omnikassa/profiles/edit/{payment_omnikassa_profile}",
 *     "collection" = "/admin/config/services/payment/payment_omnikassa/profiles",
 *     "edit-form" = "/admin/config/services/payment/payment_omnikassa/profiles/edit/{payment_omnikassa_profile}",
 *     "delete-form" = "/admin/config/services/payment/payment_omnikassa/profiles/edit/{payment_omnikassa_profile}/delete"
 *   }
 * )
 */
class PaymentOmnikassaProfile extends ConfigEntityBase implements PaymentOmnikassaProfileInterface {

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\entityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity's unique machine name.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name.
   *
   * @var string
   */
  protected $label;

  /**
   * The Rabo Omnikassa payment types that are selected.
   *
   * @var array
   */
  protected $paymentTypes;

  /**
   * The Rabo Omnikassa signing token.
   *
   * @var string
   */
  protected $signingToken;

  /**
   * The Rabo Omnikassa signing key.
   *
   * @var string
   */
  protected $signingKey;

  /**
   * The Rabo Omnikassa server to use.
   *
   * @var string
   */
  protected $serverType;

  /**
   * The typed config manager.
   *
   * @var \Drupal\Core\Config\TypedConfigManagerInterface
   */
  protected $typedConfigManager;

  /**
   * The entity's UUID.
   *
   * @var string
   */
  public $uuid;

  /**
   * Set the id.
   */
  public function setId($id) {
    $this->id = $id;

    return $this;
  }

  /**
   * Set the label.
   */
  public function setLabel($label) {
    $this->label = $label;

    return $this;
  }

  /**
   * Set the payment types.
   */
  public function setPaymentTypes($paymentTypes) {
    $this->paymentTypes = $paymentTypes;

    return $this;
  }

  /**
   * Get the payment types.
   */
  public function getPaymentTypes(): array {
    // This should always return an array, not NULL.
    if (!empty($this->paymentTypes)) {
      return $this->paymentTypes;
    }
    else {
      return [];
    }
  }

  /**
   * Set the signing token.
   */
  public function setSigningToken($signingToken) {
    $this->signingToken = $signingToken;

    return $this;
  }

  /**
   * Get the signing token.
   */
  public function getSigningToken() {
    return $this->signingToken;
  }

  /**
   * Set the signing key.
   */
  public function setSigningKey($signingKey) {
    $this->signingKey = $signingKey;

    return $this;
  }

  /**
   * Get the signing key.
   */
  public function getSigningKey() {
    return $this->signingKey;
  }

  /**
   * Set the server type.
   */
  public function setServerType($serverType) {
    $this->serverType = $serverType;

    return $this;
  }

  /**
   * Get the server type.
   */
  public function getServerType() {
    return $this->serverType;
  }

  /**
   * Sets the entity manager.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   *
   * @return $this
   */
  public function setEntityTypeManager(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  protected function entityTypeManager() {
    if (!$this->entityTypeManager) {
      $this->entityTypeManager = parent::entityTypeManager();
    }

    return $this->entityTypeManager;
  }

  /**
   * Sets the typed config.
   *
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typed_config_manager
   *   The typed config manager.
   *
   * @return $this
   */
  public function setTypedConfig(TypedConfigManagerInterface $typed_config_manager) {
    $this->typedConfigManager = $typed_config_manager;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  protected function getTypedConfig() {
    if (!$this->typedConfigManager) {
      $this->typedConfigManager = parent::getTypedConfig();
    }

    return $this->typedConfigManager;
  }

}
