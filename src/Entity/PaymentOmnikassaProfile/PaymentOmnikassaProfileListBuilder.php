<?php

namespace Drupal\payment_omnikassa\Entity\PaymentOmnikassaProfile;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;

/**
 * Lists Rabo Omnikassa profile entities.
 */
class PaymentOmnikassaProfileListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Label');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\payment_omnikassa\Entity\PaymentOmnikassaProfile */

    $row['label'] = $entity->label();

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = parent::render();

    $build['#empty'] = $this->t('There are no Rabo Omnikassa profiles configured yet.');

    return $build;
  }

}
