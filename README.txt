## CONTENTS OF THIS FILE

 * Introduction
 * Requirements
 * Installation
 * Configuration


## INTRODUCTION

The module provides

A payment provider for the Payment module (https://www.drupal.org/project/payment).

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/payment_omnikassa

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/payment_omnikassa


## REQUIREMENTS

This module requires the Payment module, which requires the Currency and core-Views modules.
It also requires the omnikassa2-sdk php library. This is installed automatically if you
install with composer.


## INSTALLATION

 * Install the Payment Rabo Omnikassa module as you would normally install a
   contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
   further information.


## CONFIGURATION

Prequisites:
  * Make shure you have done some core-Payment setup, like adding a Currency.

Configuration of the Rabo Omnikassa Payment module:
    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Configuration > Webservices > Payment >
         Rabo Omnikassa Payment > Rabo Omnikassa Profiles
    3. Add a Rabo Omnikassa Profile.
    4. Navigate to Administration > Configuration > Webservices > Payment >
         Payment Methods
    5. Add a Payment Method of the type Rabo Omnikassa.
         Select the Rabo Omnikassa profile we created in the previous step.

You are ready to use the Rabo Omnikassa Payment provider now with modules
that implement the Payment module.

@TODO:
* Add choice of payment method form for customer
* Add tests
